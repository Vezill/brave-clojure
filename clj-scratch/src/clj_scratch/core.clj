(ns clj-scratch.core
  (:gen-class)
  (:require [clojure.string :as str]))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

;;; Scratch file!
;;; Mainly compiling forms, not really the project itself
(conj '(1 2 3 4) 5)
(conj [1 2 3 4] 5)
(hash-set 1 1 2 2)
(conj #{:a :b} :a)
(set [3 3 3 4 4])
(contains? #{:a :b :c :d} :a)
(contains? [:a :b :c :d] :a)
(or + -)
((or + -) 1 2 3)
((and (= 1 1) +) 1 2 3)
((first [+ 0]) 1 2 3)

;; Exceptions
;; (1 2 3 4)
;; ("test" 1 2 3)

(map inc [0 1 2 3])

(defn weird-arity
  ([]
   "How could you do this to us?")
  ([number]
   (inc number)))

(defn favourite-things
  [name & things]
  (str "Hello, " name ", here are my favourite things: "
       (str/join ", " things)))

(defn chooser
  [[first-choice second-choice & unimportant-choices]]
  (println (str "Your first choice is: " first-choice))
  (println (str "Your second choice is: " second-choice))
  (println (str "We're ignoring the rest of your choices. "
                "Here they are in case you need to cry over them: "
                (str/join ", " unimportant-choices))))

(defn announce-treasure-location
  [{lat :lat lng :lng}]
  (println (str "Treasure lat: " lat))
  (println (str "Treasure lng: " lng)))

(defn announce-treasure-location-keys
  [{:keys [lat lng]}]
  (println (str "Treasure lat: " lat))
  (println (str "Treasure lng: " lng)))

(defn announce-treasure-location-keys-keep
  [{:keys [lat lng] :as treasure-location}]
  (println (str "Treasure lat: " lat))
  (println (str "Treasure lng: " lng)))

(map (fn [name] (str "Hi, " name))
     ["Darth Vader" "Mr. Magoo"])

(#(* % 3) 8)
(#(str %1 " and " %2) "cornbread" "butter beans")
(#(identity %&) 1 "blarg" :yip) ;; %& is a rest param

(map str ["a" "b" "c"] ["A" "B" "C"])
;; equivalent to: (list (str "a" "A") (str "b" "B") (str "c" "C"))

(def human-consumption [8.1 7.3 6.6 5.0])
(def critter-consumption [0.0 0.2 0.3 1.1])

(defn unify-diet-data [human critter]
  {:human human
   :critter critter})

(map unify-diet-data human-consumption critter-consumption)

(def sum #(reduce + %))
(def avg #(/ (sum %) (count %)))

(defn stats [numbers]
  (map #(% numbers) [sum count avg]))

(stats [3 4 10])
;; => (17 3 17/3)
(stats [80 1 44 13 6])
;; => (144 5 144/5)

;; Using map to retrieve value associated with a keyword
(def identities
  [{:alias "Batman" :real "Bruce Wayne"}
   {:alias "Spiderman" :real "Peter Parker"}
   {:alias "Santa" :real "Your Mom"}
   {:alias "Easter Bunny" :real "Your Dad"}])

(map :real identities)

(reduce (fn [new-map [key val]]
          (assoc new-map key (inc val)))
        {}
        {:max 30 :min 10})

;; equivalent to (assoc (assoc {} :max (inc 30)) :min (inc 10))

;; Using reduce to filter keys
(reduce (fn [new-map [key val]]
          (if (> val 4)
            (assoc new-map key val)
            new-map))
        {}
        {:human 4.1 :critter 3.9})

(take 3 [1 2 3 4 5 6 7 8 9 0])
(drop 3 [1 2 3 4 5 6 7 8 9 0])

(def food-journal
  [{:month 1 :day 1 :human 5.3 :critter 2.3}
   {:month 1 :day 2 :human 5.1 :critter 2.0}
   {:month 2 :day 1 :human 4.9 :critter 2.1}
   {:month 2 :day 2 :human 5.0 :critter 2.5}
   {:month 3 :day 1 :human 4.2 :critter 3.3}
   {:month 3 :day 2 :human 4.0 :critter 3.8}
   {:month 4 :day 1 :human 3.7 :critter 3.9}
   {:month 4 :day 2 :human 3.7 :critter 3.6}])

;; Get data for Jan & Fed
(take-while #(< (:month %) 3) food-journal)
;; Get data for Mar & Apr
(drop-while #(< (:month %) 3) food-journal)
;; Get data for Feb & Mar
(take-while #(< (:month %) 4)
            (drop-while #(< (:month %) 2) food-journal))

(filter #(< (:human %) 5) food-journal)
;; While this works instead of the above, filter will process all your data instead of terminating
;; early. Which one you use depends on the situation.
(filter #(< (:month %) 3) food-journal)

;; Some will also terminate early on the first truthy value
(some #(> (:critter %) 5) food-journal)
(some #(> (:critter %) 3) food-journal)

;; Some returns the value of the fn passed to it
(some #(and (> (:critter %) 3) %) food-journal)

(sort [3 1 2])

;; Sort has default sorting depending on the type
(sort ["aaa" "bb" "c"])
;; Use sort-by to define how to sort the seq
(sort-by count ["aaa" "c" "bb"])

(concat [1 2] [3 4])
;; Can also work with different collections as they become seqs
;; Note: because sets are unordered, there's no guarantee that it will be concated in the order provided
(concat [1 2] '(3 4) #{5 6})

(def vampire-database
  {0 {:makes-blood-puns? false :has-pulse? true :name "McFishwich"}
   1 {:makes-blood-puns? false :has-pulse? true :name "McMackson"}
   2 {:makes-blood-puns? true :has-pulse? false :name "Damon Salvator"}
   3 {:makes-blood-puns? true :has-pulse? true :name "Mickey Mouse"}})

(defn vampire-related-details
  [social-security-number]
  (Thread/sleep 1000)
  (get vampire-database social-security-number))

(defn vampire?
  [record]
  (and (:makes-blood-puns? record)
       (not (:has-pulse? record))
       record))

(defn identify-vampire
  [social-security-numbers]
  (first (filter vampire?
                 (map vampire-related-details social-security-numbers))))

(time (vampire-related-details 0))

;; Many seq functions are 'lazy', and don't actualy realize values until they are accessed
(time
 (def mapped-details (map vampire-related-details (range 0 100000))))
;; That being said, clojure 'chunks' it's lazy data, so it realized the first 32 values to
;; improve future performance
(time (first mapped-details))
(time (identify-vampire (range 0 1000000)))

(concat (take 8 (repeat "na")) ["Batman!"])
(take 3 (repeatedly #(rand-int 10)))

(defn even-numbers
  ([] (even-numbers 0))
  ([n] (cons n (lazy-seq (even-numbers (+ n 2))))))

(take 10 (even-numbers))

(map identity {:sunlight-reaction "Glitter!"})
(into {} (map identity {:sunlight-reaction "Glitter!"}))

(into {:favourite-emotion "gloomy"} [[:sunlight-reaction "Glitter!"]])
(into ["cherry"] '("pine" "spruce"))

(conj {:time "Midnight"} [:place "ye olde cemetarium"])

(max 0 1 2)
(apply max [0 1 2])

;; Partial gives a fn back with some of it's arguments already supplied
(def add-10 (partial + 10))
(add-10 3)

(def add-missing-elements
  (partial conj ["water" "earth" "air"]))
(add-missing-elements "unobtainium" "adamantium")

(defn lousy-logger
  [log-level message]
  (condp = log-level
    :warn (str/lower-case message)
    :emergency (str/upper-case message)))

(def warn (partial lousy-logger :warn))
(def emergency (partial lousy-logger :emergency))

(warn "Red light ahead")
(lousy-logger :warn "Red light ahead")

;; Complement, or the negation of a boolean function
;; (defn identify-humans
;;   [social-security-number]
;;   (filter #(not (vampire? %))
;;           (map vampire-related-details social-security-number)))
;; Can also be done as
(def not-vampire? (complement vampire?))
(defn identify-humans
  [social-security-number]
  (filter not-vampire?
          (map vampire-related-details social-security-number)))

(defn criticize-code
  [criticism code]
  `(println ~criticism (quote ~code)))

(defmacro code-critic
  [{:keys [good bad]}]
  `(do ~@(map #(apply criticize-code %)
              [["Sweet lion of Zion, this is bad code:" bad]
               ["Great cow of Moscow, this is good code:" good]])))

;; Character creation and stat access macros
(def character
  {:name "Smooches McCutes"
   :attributes {:intelligence 10
                :strength 4
                :dexterity 5}})

;; (def c-int (comp :intelligence :attributes))
;; (def c-str (comp :strength :attributes))
;; (def c-dex (comp :dexterity :attributes))

(defmacro defattrs
  [& attrs]
  `(do ~@(for [[name# attr#] (partition 2 attrs)]
           `(def ~name# (comp ~attr# :attributes)))))

;; Futures and Delays
(def gimli-kills
  ["serious.jpg" "fun.jpg" "playful.jpg"])

(defn email-user
  [email-address]
  (println "Sending kill notification to" email-address))

(defn upload-document
  "Needs to be implemented."
  [headshot]
  true)

(let [notify (delay (email-user "and-my-axe@gmail.com"))]
  (doseq [kill gimli-kills]
    (future
      (upload-document kill)
      (force notify))))

;; Promises
(def yak-butter-international
  {:store "Yak Butter International"
   :price 90
   :smoothness 90})
(def butter-than-nothing
  {:store "Butter Than Nothing"
   :price 150
   :smoothness 83})
(def baby-got-yak
  {:store "Baby Got Yak"
   :price 94
   :smoothness 99})

(defn mock-api-call
  [result]
  (Thread/sleep 1000)
  result)

(defn satisfactory?
  "If the butter meets our criteria, return the butter, else return false."
  [butter]
  (and (<= (:price butter) 100)
       (>= (:smoothness butter) 97)
       butter))

;; Single threaded
(time
 (some (comp satisfactory? mock-api-call)
       [yak-butter-international butter-than-nothing baby-got-yak]))

;; Using promises and futures
(time
 (let [butter-promise (promise)]
   (doseq [butter [yak-butter-international butter-than-nothing baby-got-yak]]
     (future (if-let [satisfactory-butter (satisfactory? (mock-api-call butter))]
               (deliver butter-promise satisfactory-butter))))
   (println "And the winner is:" (deref butter-promise 3000 "none"))))

;; Using promises for "callbacks"
(let [ferengi-wisdom-promise (promise)]
  (future (println "Here's some Ferengi wisdom:" @ferengi-wisdom-promise))
  (Thread/sleep 100)
  (deliver ferengi-wisdom-promise "Whisper your way to success."))

;; Splitting up a task into concurrent and serial portions
(defmacro wait
  "Sleep `timeout` seconds before evaluating body."
  [timeout & body]
  `(do (Thread/sleep ~timeout) ~@body))

(let [saying3 (promise)]
  (future (deliver saying3 (wait 100 "Cheerio!")))
  @(let [saying2 (promise)]
     (future (deliver saying2 (wait 400 "Pip pip!")))
     @(let [saying1 (promise)]
        (future (deliver saying1 (wait 200 "Ello, gov'na!")))
        (println @saying1)
        saying1)
     (println @saying2)
     saying2)
  (println @saying3)
  saying3)

(defmacro enqueue
  ([q concurrent-promise-name concurrent serialized]
   `(let [~concurrent-promise-name (promise)]
      (future (deliver ~concurrent-promise-name ~concurrent))
      (deref ~q)
      ~serialized
      ~concurrent-promise-name))
  ([concurrent-promise-name concurrent serialized]
   `(enqueue (future) ~concurrent-promise-name ~concurrent ~serialized)))

(time
 @(-> (enqueue saying (wait 200 "Ello, gov'na!") (println @saying))
      (enqueue saying (wait 400 "Pip pip!") (println @saying))
      (enqueue saying (wait 100 "Cheerio!") (println @saying))))

;; Using Atoms
;; Storing a thread-safe state
(def fred
  (atom {:cuddle-hunger-level 0
         :percent-deteriorated 0}))

;; Get the value of the atom
@fred

(let [zombie-state @fred]
  (if (>= (:percent-deteriorated zombie-state) 50)
    (future (println (:cuddle-hunger-level zombie-state)))))

;; Set a new value to an atom, retrying if the value has changed
;; (swap! fred
;;        (fn [current-state]
;;          (merge-with + current-state {:cuddle-hunger-level 1
;;                                       :percent-deteriorated
;; More readable:
(swap! fred update-in [:cuddle-hunger-level] + 10)

;; State can be captured
(let [num (atom 1)
      s1 @num]
  (swap! num inc)
  (println "State 1:" s1)
  (println "Current state:" @num))

;; And reset
(reset! fred {:cuddle-hunger-level 0
              :percent-deteriorated 0})

;; Watches and validators can be added
(defn shuffle-speed
  [zombie]
  (* (:cuddle-hunger-level zombie)
     (- 100 (:percent-deteriorated zombie))))

;; Watch functions take 4 args:
;;   key - a keyword for reporting use
;;   watched - the atom being watched
;;   old-state - the previous state of the atom
;;   new-state - the new state of the atom
(defn shuffle-alert
  [key watched old-state new-state]
  (let [sph (shuffle-speed new-state)]
    (if (> sph 5000)
      (do
        (println "Run, you fool!")
        (println "The zombie's SPH is now" sph)
        (println "This message brought to your courtesy of" key))
      (do
        (println "All's well with" key)
        (println "Cuddle hunger:" (:cuddle-hunger-level new-state))
        (println "Percent deteriorated:" (:percent-deteriorated new-state))
        (println "SPH:" sph)))))

(reset! fred {:cuddle-hunger-level 22
              :percent-deteriorated 2})

(add-watch fred :fred-shuffle-alert shuffle-alert)
(swap! fred update-in [:percent-deteriorated] + 1)

(swap! fred update-in [:cuddle-hunger-level] + 30)

;; Validators can be added onto the atom at creation
(defn percent-deteriorated-validator
  [{:keys [percent-deteriorated]}]
  (and (>= percent-deteriorated 0)
       (<= percent-deteriorated 100)))

(def bobby
  (atom {:cuddle-hunger-level 0 :percent-deteriorated 0}
        :validator percent-deteriorated-validator))

(swap! bobby update-in [:percent-deteriorated] + 200)
;; Will throw an `IllegalStateException`

;; You can also add more exact exceptions
;; (defn percent-deteriorated-validator
;;   [{:keys [percent-deteriorated]}]
;;   (or (and (>= percent-deteriorated 0)
;;            (<= percent-deteriorated 100))
;;       (throw (IllegalStateException. "That's not mathy!"))))

;; (def bobby
;;   (atom {:cuddle-hunger-level 0 :percent-deteriorated 0}
;;         :validator percent-deteriorated-validator))

;; (swap! bobby update-in [:percent-deteriorated] + 200)

;; Refs are similar to atoms, but they have to be in a `dosync` transaction
(def sock-varieties
  #{"darned" "argyle" "wool" "horsehair" "mulleted" "passive-aggressive" "striped" "polka-dotted"
    "athletic" "business" "power" "invisible" "gollumed"})

(defn sock-count
  [sock-variety count]
  {:variety sock-variety
   :count count})

(defn generate-sock-gnome
  "Create an initial sock gnome state with no socks."
  [name]
  {:name name
   :socks #{}})

(def sock-gnome (ref (generate-sock-gnome "Barumpharumph")))
(def dryer (ref {:name "LG 1337"
                 :socks (set (map #(sock-count % 2) sock-varieties))}))

(:socks @dryer)

(defn steal-sock
  [gnome dryer]
  (dosync
   (when-let [pair (some #(if (= (:count %) 2) %) (:socks @dryer))]
     (let [updated-count (sock-count (:variety pair) 1)]
       (alter gnome update-in [:socks] conj updated-count)
       (alter dryer update-in [:socks] disj pair)
       (alter dryer update-in [:socks] conj updated-count)))))

(steal-sock sock-gnome dryer)
(:socks @sock-gnome)

(defn similar-socks
  [target-sock sock-set]
  (filter #(= (:variety %) (:variety target-sock)) sock-set))

(similar-socks (first (:socks @sock-gnome)) (:socks @dryer))

;; Refs use `alter` and `commute` to update their values:
;; `alter`:
;;   1. Reach outside the transaction and read the ref's current state.
;;   2. Compare the current state to the state the ref started within the transaction.
;;   3. If the two differ, make the transaction retry.
;;   4. Otherwise, commit the altered ref state.
;; `commute`:
;;   1. Reach outside the transaction and read the ref's current state.
;;   2. Run the `commute` function again using the current state.
;;   3. Commit the result.
;; `commute` never forces the transaction to retry, which can improve performance but should
;; only be used when you're sure it's not possibel for your refs to end up in an invalid state.

;; Variables can be made `dynamic` to allow rebinding in a scope, and must have earmuffs `**`
(def ^:dynamic *notification-address* "dobby@elf.org")

(binding [*notification-address* "test@elf.org"]
  (println *notification-address*)
  (binding [*notification-address* "tester-2@elf.org"]
    (println *notification-address*))
  (println *notification-address*))

;; `thread-bound?` can be used to check if a dynamic var was bound
(def ^:dynamic *troll-thought* nil)
(defn troll-riddle
  [your-answer]
  (let [number "man meat"]
    (when (thread-bound? #'*troll-thought*)
      (set! *troll-thought* number))
    (if (= number your-answer)
      "TROLL: You can cross the bridge!"
      "TROLL: Time to eat you, succulent human!")))

(binding [*troll-thought* nil]
  (println (troll-riddle 2))
  (println "SUCCULENT HUMAN: Oooooh! the answer was" *troll-thought*))

;; While it's generally not a good idea to ever do this, you can alter a var with `alter-var-root`
(def power-source "hair")
(alter-var-root #'power-source (fn [_] "7-eleven parking lot"))

;; And you can temporarily alter a vars root with `with-redefs`
(with-redefs [*out* *out*]
  (doto (Thread. #(println "with redefs allows me to show up in the REPL"))
    .start
    .join))

;; Using parallel map to speed up data manipulation
(def alphabet-length 26)

;; Vector of chars, A-Z
(def letters
  (mapv (comp str char (partial + 65)) (range alphabet-length)))

(defn random-string
  "Returns a random string of specified length."
  [length]
  (apply str (take length (repeatedly #(rand-nth letters)))))

(defn random-string-list
  [list-length string-length]
  (doall (take list-length (repeatedly (partial random-string string-length)))))

;; While in this case the parallel map is facter
(def orc-names (random-string-list 3000 7000))
(time (dorun (map str/lower-case orc-names)))
(time (dorun (pmap str/lower-case orc-names)))

;; Because of the overhead of using parallel threads, this one is slower
(def orc-name-abbrevs (random-string-list 20000 300))
(time (dorun (map str/lower-case orc-name-abbrevs)))
(time (dorun (pmap str/lower-case orc-name-abbrevs)))

;; As a solution, you can partition the data so that there are less threads running more things
;; Example of using a partitioned list to optimize parallel maps
(defn ppmap
  "Partitioned pmap, for grouping map ops together to make parallel overhead worthwhile."
  [grain-size f & colls]
  (apply concat
         (apply pmap
                (fn [& pgroups] (doall (apply map f pgroups)))
                (map (partial partition-all grain-size) colls))))

(time (dorun (ppmap 1000 str/lower-case orc-name-abbrevs)))

;; Lets get a bunch of quotes!
(def random-quotes
  (atom #{}))

(reset! random-quotes #{})

(defn get-quotes
  [num-quotes]
  (dotimes [_ num-quotes]
    (future (swap! random-quotes conj (slurp "https://braveclojure.com/random-quote")))))

(get-quotes 500)

;; trying to impl (frequencies words)
(defn count-words
  [words]
  (reduce #(assoc %1 %2 (inc (%1 %2 0))) {} words))

(let [quotes @random-quotes
      everything (str/join " " quotes)
      cleaned (-> everything
                  str/lower-case
                  (str/split #" "))
      words (map (fn [word] (apply str (filter #(Character/isAlphabetic (int %)) word))) cleaned)]
  (count-words (filter #(> (count %) 1) words)))

;; Lets heal our ally
(defmacro defchar
  [name & {:keys [strength agility intelligence max-hp max-mp inventory]
           :or {strength 10 agility 10 intelligence 10 max-hp 40 max-mp 20 inventory {}}}]
  `(def ~name
     (ref {:name (str/capitalize (str (quote ~name)))
           :current-hp ~max-hp
           :max-hp ~max-hp
           :current-mp ~max-mp
           :max-mp ~max-mp
           :strength ~strength
           :agility ~agility
           :intelligence ~intelligence
           :inventory ~inventory})))

(defn has-item?
  [character item]
  (contains? (:inventory @character) item))

(defn use-item
  [character item]
  (alter character update-in [:inventory item] dec)
  (when (= (item (:inventory @character)) 0)
    (alter character update :inventory dissoc item)))

(defn heal
  [character amount]
  (alter character update :current-hp + amount)
  (when (> (:current-hp @character) (:max-hp @character))
    (alter character assoc :current-hp (:max-hp @character))))

(defn damage
  [character amount])

(defn use-potion
  [user to-heal]
  (if-not (has-item? user :health-potion)
    (println (:name @user) "doesn't have a health potion to use on" (:name @to-heal) "!")
    (dosync
     (use-item user :health-potion)
     (heal to-heal 20)
     (println (:name @user) "healed" (:name @to-heal) "for 20 points!"))))


(defchar john)
(dosync
 (alter john assoc :current-hp 10))

(defchar bob
  :inventory {:health-potion 2})

(use-potion john bob) ;; shouldn't work
(use-potion bob john)
(use-potion bob john)
(use-potion bob john) ;; will have used all our potions by this point
