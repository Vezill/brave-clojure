(ns fwpd.core
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:gen-class))

(def vamp-keys [:name :glitter-index])

(defn str->int
  [str]
  (Integer/parseInt str))

(def conversions
  {:name identity
   :glitter-index str->int})

(defn convert
  [vamp-key value]
  ((get conversions vamp-key) value))

(defn parse
  "Convert a CSV string into rows of columns."
  [csv]
  (map #(str/split % #",") (str/split csv #"(\r)?\n")))

(defn mapify
  "Return a seq of maps like {:name \"Edward Cullen\" :glitter-index 10}."
  [rows]
  (map (fn [unmapped-row]
         (reduce (fn [row-map [vamp-key value]]
                   (assoc row-map vamp-key (convert vamp-key value)))
                 {}
                 (map vector vamp-keys unmapped-row)))
       rows))

(defn glitter-filter
  [min-glitter records]
  (filter #(>= (:glitter-index %) min-glitter) records))

(defn -main
  [& args]
  (let [suspects (slurp (io/resource "./suspects.csv"))]
    (->> suspects
         parse
         mapify
         (glitter-filter 3))))
